﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework7.Common
{
    internal interface ISumCalculator
    {
        public void CalculateSum();
    }
}
