﻿// See https://aka.ms/new-console-template for more information
using homework7;
using homework7.Calculators;
using homework7.Common;

namespace homework7
{
    class Program
    {
        static void Main(string[] args)
        {
            ShowStatisticsForNumbers(100000);
            ShowStatisticsForNumbers(1000000);
            ShowStatisticsForNumbers(10000000);
        }

        static void ShowStatisticsForNumbers(int numbersCount)
        {
            var numbers = IntegerElementsCreator.GetGeneratedArray(numbersCount);

            ISumCalculator defaultSumCalculator = new DefaultSumCalculator(numbers);
            ISumCalculator plinqSumCalculator = new PlinqSumCalculator(numbers);
            ISumCalculator threadSumCalculator = new ThreadSumCalculator(numbers, 4);

            Console.WriteLine("Количество элементов в массиве: ", numbersCount);
            Console.WriteLine("Обычный: " + TimeObserver.GetExecutionTime(() => defaultSumCalculator.CalculateSum()));
            Console.WriteLine("Thread: " + TimeObserver.GetExecutionTime(() => plinqSumCalculator.CalculateSum()));
            Console.WriteLine("PLINQ: " + TimeObserver.GetExecutionTime(() => threadSumCalculator.CalculateSum()));
        }
    }
}