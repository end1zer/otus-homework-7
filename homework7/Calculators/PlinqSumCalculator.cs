﻿using homework7.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework7.Calculators
{
    internal class PlinqSumCalculator : ISumCalculator
    {
        private readonly int[] _array;

        public PlinqSumCalculator(int[] array)
        {
            _array = array;
        }

        public void CalculateSum()
        {
            _array.AsParallel().Sum();
        }
    }
}
