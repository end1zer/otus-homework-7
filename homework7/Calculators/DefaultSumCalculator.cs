﻿using homework7.Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework7.Calculators
{
    internal class DefaultSumCalculator : ISumCalculator
    {
        private readonly int[] _array;

        public DefaultSumCalculator(int[] array)
        {
            _array = array;
        }

        public void CalculateSum()
        {
            _array.Sum();
        }
    }
}
