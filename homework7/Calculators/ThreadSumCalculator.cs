﻿using homework7.Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework7.Calculators
{
    internal class ThreadSumCalculator : ISumCalculator
    {
        private readonly int[] _array;
        private readonly int _chunks;

        public ThreadSumCalculator(int[] array, int chunks)
        {
            _array = array;
            _chunks = chunks;
        }

        public void CalculateSum()
        {
            ThreadSum(SplitArray(), new Task<int>[_chunks]);
        }

        private IEnumerable<int[]> SplitArray()
        {
            return _array.Chunk(_array.Length / _chunks).Select(t => t.ToArray());
        }

        private static int ThreadSum(IEnumerable<int[]> chunks, Task<int>[] taskArray)
        {
            var i = 0;
            foreach (var chunk in chunks)
            {
                taskArray[i] = Task.Run(() => chunk.Sum());
                i++;
            }
            Task.WaitAll(taskArray);
            return taskArray.Sum(t => t.Result);
        }
    }
}
