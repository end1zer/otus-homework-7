﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework7
{
    internal static class IntegerElementsCreator
    {
        public static int[] GetGeneratedArray(int length)
        {
            int[] _array = new int[length];
            var random = new Random();
            for (int i = 0; i < length; i++)
            {
                _array[i] = random.Next(-1000, 1000);
            }
            return _array;
        }
    }
}
